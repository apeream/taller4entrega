function loadScript(callback) {
  var s = document.createElement('script');
  s.src = 'https://rawgithub.com/marmelab/gremlins.js/master/gremlins.min.js';
  if (s.addEventListener) {
    s.addEventListener('load', callback, false);
  } else if (s.readyState) {
    s.onreadystatechange = callback;
  }
  document.body.appendChild(s);
}

function unleashGremlins(ttl, callback) {
  function stop() {
    horde.stop();
    callback();
  }
  var horde = window.gremlins.createHorde()
    //Modifique la especie que llena formularios para que solo intente 
    //actuar sobre elementos que se pueden llenar.
    .gremlin(gremlins.species.formFiller()
      .canFillElement(function(element) {
        return $(element).parents('.fill-me-in').length;
      }))
    //Modifique la especie que realiza clicks para que solo haga 
    //clicks sobre botones o links
    .gremlin(gremlins.species.clicker()
      .clickTypes(['click'])
      .canClick(function(element) {
        // only click elements in bar
        return $(element).parents('button').length || 
          $(element).parents('a').length;
        // when canClick returns false, the gremlin will look for another
        // element to click on until maxNbTries is reached
      }))
    .gremlin(gremlins.species.toucher())
    .gremlin(gremlins.species.scroller())    
    .gremlin(gremlins.species.typer())
    //Intente arreglar el comportamiento de los gremlins para que no ejecute acciones en elementos que ya no se encuentran en el documento.
    ;
  //Cambie la distribucíon para darle prioridad al gremlin que realiza clicks
  horde.strategy(gremlins.strategies.distribution()
    .delay(50)
    .distribution([0.2, 0.3, 0.2, 0.2, 0.1])
  )
  horde.seed(1234);

  horde.after(callback);
  window.onbeforeunload = stop;
  setTimeout(stop, ttl);
  horde.unleash();
}

describe('Monkey testing with gremlins ', function() {

  it('it should not raise any error', function() {
    browser.url('/');
    browser.click('button=Cerrar');

    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(loadScript);

    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(unleashGremlins, 50000);
  });

  afterAll(function() {
    browser.log('browser').value.forEach(function(log) {
      browser.logger.info(log.message.split(' ')[2]);
    });
  });

});