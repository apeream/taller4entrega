describe('Los estudiantes under monkeys', function () {
    it('visits los estudiantes and survives monkeys', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        randomEvent(10);
    })
})

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function randomEvent(monkeysLeft) {
    var monkeysLeft = monkeysLeft;
    if (monkeysLeft > 0) {
        cy.log('Monkeys Left: ' + monkeysLeft);
        //Seleccionar un evento al azar entre estos:
        var event = getRandomInt(0, 4);
        switch (event) {
            case 0:
                //Hacer click en un link al azar
                randomLink(monkeysLeft);
                break;

            case 1:
                //Llenar un campo de texto al azar
                randomInputText(monkeysLeft);
                break;

            case 2:
                //Seleccionar un combo al azar
                randomSelect(monkeysLeft);
                break;

            case 3:
                //Hacer click en un botón al azar
                randomButton(monkeysLeft);
                break;

            default:
                break;
        }
    };
}

function randomLink(monkeysLeft) {
    cy.log('Link al azar');
    var monkeysLeft = monkeysLeft;
    cy.get('body').then(($body) => {
        if ($body.find('a').length) {
            cy.get('a').then($links => {
                var randomLink = $links.get(getRandomInt(0, $links.length));
                if (!Cypress.Dom.isHidden(randomLink)) {
                    cy.wrap(randomLink).click({ force: true });
                }
            })
        }
        monkeysLeft = monkeysLeft - 1;
        setTimeout(randomEvent, 1000, monkeysLeft);
    })
}

function randomInputText(monkeysLeft) {
    cy.log('Input text al azar');
    var monkeysLeft = monkeysLeft;
    cy.get('body').then(($body) => {
        if ($body.find('input[type="text"]').length) {
            cy.get('input[type="text"]').then($texts => {
                var randomText = $texts.get(getRandomInt(0, $texts.length));
                if (!Cypress.Dom.isHidden(randomText)) {
                    cy.wrap(randomText).click({ force: true }).type("test");
                }
            })
        }
        monkeysLeft = monkeysLeft - 1;
        setTimeout(randomEvent, 1000, monkeysLeft);
    })
}

function randomSelect(monkeysLeft) {
    cy.log('Select al azar');
    var monkeysLeft = monkeysLeft;
    cy.get('body').then(($body) => {
        if ($body.find('select').length) {
            cy.get('select').then($selects => {
                var ramdomSelect = $selects.get(getRandomInt(0, $selects.length));
                if (!Cypress.Dom.isHidden(ramdomSelect)) {
                    cy.wrap(ramdomSelect).find('option').first({ force: true });
                };
            });
        }
        monkeysLeft = monkeysLeft - 1;
        setTimeout(randomEvent, 1000, monkeysLeft);
    })
}

function randomButton(monkeysLeft) {
    cy.log('Button al azar');
    var monkeysLeft = monkeysLeft;
    cy.get('body').then(($body) => {
        if ($body.find('button').length) {
            cy.get('button').then(buttons => {
                var ramdomButtons = buttons.get(getRandomInt(0, buttons.length));
                if (!Cypress.Dom.isHidden(ramdomButtons)) {
                    cy.wrap(ramdomButtons).click({ force: true });
                };
            });
        }
        monkeysLeft = monkeysLeft - 1;
        setTimeout(randomEvent, 1000, monkeysLeft);
    })
}